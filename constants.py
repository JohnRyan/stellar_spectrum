import math
# Constants
# Physical Constants (cgs)
e = 4.8032068e-10   # yt.units.esu
c = 2.99792458e10   # yt.units.cm/yt.units.s
h = 6.6260755e-27   # yt.units.erg*yt.units.s
Ry = 2.1798741e-11  # yt.units.erg
kB = 1.380658e-16   # yt.units.erg/yt.units.K
me = 9.1093897e-28  # yt.units.g
mH = 1.6733e-24     # yt.units.g
eV = 1.6021772e-12  # yt.units.erg
G = 6.67259e-8      # yt.units.cm**3/yt.units.g/yt.units.s**2
# Unit conversions
Mb = 1.e-18      # yt.units.cm
AA = 1.e-8       # yt.units.cm
au = 1.496e13    # yt.units.cm
pc = 3.086e18    # yt.units.cm
ns = 1e-9        # yt.units.s
day = 24.*3600.  # yt.units.s
year = 31557600  # yt.units.s
bar = 1e6        # yt.units.barye
# Derived Constants
hc = h*c
a0 = h**2./(4.*math.pi**2.*me*e**2.)  # yt.units.cm
sSB = 2*math.pi**5*kB**4/(15*c**2*h**3)
# Lyman Alpha (Doublet Mean)
Lya_wl = 1215.67e-8  # yt.units.cm
Lya_nu = c/Lya_wl
Lya_f12 = .4164
Lya_gamma = 6.265e8  # 1/yt.units.s
# Clestial bodies values
Rjupiter = 7.140e9
Mjupiter = 1.899e30
Rearth = 6.378e8
Mearth = 5.976e27
Rsun = 6.96e10
Msun = 1.99e33
Lsun = 3.9e33
