import matplotlib.pyplot as plt
# Matplotlib rcParams config
# figure
plt.rcParams['figure.figsize'] = (9.0, 7.0)
plt.rcParams['figure.dpi'] = 100
# font
plt.rcParams['font.size'] = 20
# lines
plt.rcParams['lines.linewidth'] = 5
plt.rcParams['lines.markersize'] = 20
# ticks
plt.rcParams['xtick.major.size'] = 10
plt.rcParams['xtick.major.width'] = 1
plt.rcParams['xtick.minor.size'] = 5
plt.rcParams['xtick.minor.width'] = 1
plt.rcParams['ytick.major.size'] = 10
plt.rcParams['ytick.major.width'] = 1
plt.rcParams['ytick.minor.size'] = 5
plt.rcParams['ytick.minor.width'] = 1
plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'
# axes
axescolor = 'black'
plt.rcParams['axes.linewidth'] = 1.5
plt.rcParams['axes.labelcolor'] = axescolor
plt.rcParams['axes.edgecolor'] = axescolor
plt.rcParams['xtick.color'] = axescolor
plt.rcParams['ytick.color'] = axescolor
# legend
plt.rcParams['legend.handletextpad'] = 0.2
