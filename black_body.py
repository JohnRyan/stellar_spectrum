import numpy as np
import scipy.integrate as integrate
import constants as const

class BlackBody():
    def __init__(self, Temp):
        self.Temp = Temp

    def spec_plot(self, ax):
        x = np.logspace(-10, 2, 5000)
        ax.plot(x, np.pi*Planck_wl(self.Temp, x),
                label=(r'B$_\lambda$(%d K)' % (self.Temp)))

    def spec_integral(self, lob=0, upb=912e-8):
        result = 0
        x_lob = 1e-8 if lob < 1e-8 else lob
        x_upb = 1e-6 if upb > 1e-6 else upb
        result += integrate.quad(lambda x: Planck_fq(self.Temp, x),
                                 const.c/x_upb, const.c/x_lob)[0]
        while x_upb < upb:
            x_lob = x_upb
            x_upb *= 1e2
            x_upb = x_upb if upb > x_upb else upb
            result += integrate.quad(lambda x: Planck_fq(self.Temp, x),
                                     const.c/x_upb, const.c/x_lob)[0]
        return np.pi*result

def Planck_wl(T, wl):
    B = 2*const.h*const.c**2/wl**5
    return B*vec_n_Bose_Einstein(const.h*const.c/wl, 0, T)

def Planck_fq(T, nu):
    B = 2*const.h*nu**3/const.c**2
    return B*vec_n_Bose_Einstein(const.h*nu, 0, T)
    
def n_Bose_Einstein(e, mu, T):
    x = (e-mu)/(const.kB*T)
    if x > 100:
        return np.exp(-x)
    elif x < 1e-10:
        return 1./x
    else:
        return 1./(np.exp(x)-1.)
    
vec_n_Bose_Einstein = np.vectorize(n_Bose_Einstein)
